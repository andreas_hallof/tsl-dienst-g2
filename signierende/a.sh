#! /usr/bin/env bash

i=aa
openssl ecparam -name brainpoolP256r1 -genkey -out key_$i.pem
openssl req -x509 -key key_$i.pem -out cert_$i.pem -days 365 -subj "/C=DE/ST=Berlin/L=Berlin/O=gemaik/OU=PoC/CN=TSL-Signaturbestaetiger $i"

