# Bestätigung der Hashwerte durch die n aus m Bestätiger #


## zu signierender Haswert-Text ##
Beispiel (dtbs.txt):

	sha256: 2d982bd8a9d5a99d78cc8815804d809a3f3682cb6e74acc4442e80dab366c9e2

## Signieren = Haswert-Bestätigung erstellen ##
Beispiel:

	openssl smime -sign -signer cert_1.pem -inkey key_1.pem -in dtbs.txt -outform der -out bestaetigter_hashwert.p7 

## privaten ECC-Schlüssel von pem -> der

    openssl ec -in key_1.pem -outform der -out key_1.der
   
## Anzeigen der Schlüssel

    $ openssl x509 -in cert_1.pem -text -noout
    Certificate:
        Data:
            Version: 3 (0x2)
            Serial Number: 9765124251245913576 (0x8784b0c4c710a5e8)
        Signature Algorithm: ecdsa-with-SHA256
            Issuer: C=DE, ST=Berlin, L=Berlin, O=gemaik, OU=PoC, CN=TSL-Signaturbestaetiger 1
            Validity
                Not Before: Mar 16 23:15:54 2018 GMT
                Not After : Mar 16 23:15:54 2019 GMT
            Subject: C=DE, ST=Berlin, L=Berlin, O=gemaik, OU=PoC, CN=TSL-Signaturbestaetiger 1
            Subject Public Key Info:
                Public Key Algorithm: id-ecPublicKey
                    Public-Key: (256 bit)
                    pub:
                        04:04:8f:f0:46:d6:a1:9f:52:8b:6a:91:99:a9:bf:
                        9b:7c:50:8a:b7:e1:a0:2c:ec:47:92:f9:df:aa:d1:
                        97:f6:db:6b:1a:a2:0f:27:b6:1f:c0:34:d0:f1:e1:
                        71:38:c5:cc:b3:dc:ec:d0:b6:61:da:58:76:7f:fc:
                        ac:70:e4:cb:86
                    ASN1 OID: prime256v1
            X509v3 extensions:
                X509v3 Subject Key Identifier:
                    A0:80:10:D9:1E:63:D5:2C:FF:42:9C:16:12:42:01:C2:7F:D7:E3:9B
                X509v3 Authority Key Identifier:
                    keyid:A0:80:10:D9:1E:63:D5:2C:FF:42:9C:16:12:42:01:C2:7F:D7:E3:9B

                X509v3 Basic Constraints: critical
                    CA:TRUE
        Signature Algorithm: ecdsa-with-SHA256
             30:46:02:21:00:a2:6c:dd:44:d2:17:aa:4b:90:67:b7:f6:55:
             1b:c0:63:13:a4:60:69:49:29:55:d3:98:d6:ee:df:1a:e2:0c:
             f0:02:21:00:cc:95:7f:fa:ed:40:d9:1a:6d:94:52:43:9e:ad:
             73:90:33:4a:5a:40:7d:d5:5b:40:7b:81:81:ab:cb:0c:72:e5

    $ openssl ec -in key_1.pem -text
    read EC key
    Private-Key: (256 bit)
    priv:
        00:f4:83:37:b1:48:40:71:f2:fb:67:17:6b:ec:18:
        c6:a0:9d:67:44:bd:0a:97:90:a1:4d:b2:2e:b3:03:
        99:76:27
    pub:
        04:04:8f:f0:46:d6:a1:9f:52:8b:6a:91:99:a9:bf:
        9b:7c:50:8a:b7:e1:a0:2c:ec:47:92:f9:df:aa:d1:
        97:f6:db:6b:1a:a2:0f:27:b6:1f:c0:34:d0:f1:e1:
        71:38:c5:cc:b3:dc:ec:d0:b6:61:da:58:76:7f:fc:
        ac:70:e4:cb:86
    ASN1 OID: prime256v1
    writing EC key
    -----BEGIN EC PRIVATE KEY-----
    MHcCAQEEIPSDN7FIQHHy+2cXa+wYxqCdZ0S9CpeQoU2yLrMDmXYnoAoGCCqGSM49
    AwEHoUQDQgAEBI/wRtahn1KLapGZqb+bfFCKt+GgLOxHkvnfqtGX9ttrGqIPJ7Yf
    wDTQ8eFxOMXMs9zs0LZh2lh2f/yscOTLhg==
    -----END EC PRIVATE KEY-----

# Ermittlung der Änderung zu einer vorhergehenden TSL

Merker: https://www.smallsurething.com/comparing-files-in-python-using-difflib/


