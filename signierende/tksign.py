#! /usr/bin/env python3

import json, os, sys, binascii, hashlib
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization
from cryptography.exceptions import InvalidSignature
from cryptography import x509
from cryptography.x509.oid import NameOID

from tkinter import *
from tkinter import ttk
from tkinter import filedialog

def HashwertAusDatei():
    root.filename=filedialog.askopenfilename(
                        initialdir = os.getcwd(),
                        title = "Select file",
                        filetypes = (
                                        ("all files","*.*"),
                                        ("xml", "*.xml")
                        )
                  )
    if root.filename:
        print(root.filename)
        with open(root.filename,"rb") as f:
            x=f.read()
        Hashwert.set(hashlib.sha256(x).hexdigest())
        Datendatei.set(root.filename)

private_key=None;

def LoadPrivKey():
    root.filename=filedialog.askopenfilename(
                        initialdir = os.getcwd(),
                        title = "Select file",
                        filetypes = (
                                        ("all files","*.*"),
                                        ("xml", "*.xml")
                        )
                  )
    if root.filename:
        print("Schlüssel", root.filename)
        with open(root.filename,"rb") as f:
            serialized_private=f.read()
        try:
            global private_key;
            private_key = serialization.load_pem_private_key(
                serialized_private, password=None, backend=default_backend()
            )
            if isinstance(private_key, ec.EllipticCurvePrivateKey):
                Schluesseldatei.set(root.filename)
        except ValueError:
            private_key=None
            Schluesseldatei.set("None")


def Signaturerzeugung(*args):
    print(Hashwert.get())
    if private_key:
        Signaturbestaetigung= {
            'Hashwerttext' : Hashwert.get() or " ",
            'Signatur'     : '1' 
        }
        signature = private_key.sign( Signaturbestaetigung['Hashwerttext'].encode(), ec.ECDSA(hashes.SHA256()) )
        Signaturbestaetigung['Signatur']=binascii.hexlify(signature)
        Signaturbestaetigung['Signatur']=Signaturbestaetigung['Signatur'].decode()
        d=json.dumps(Signaturbestaetigung, indent=4)
        print(d)
        Output.delete('1.0','end')
        Output.insert('end',d)
    else:
        print("ich habe keinen private key.")

def writeToOutput(o, msg):
    numlines = o.index('end - 1 line').split('.')[0]
    o['state'] = 'normal'
    if numlines==4:
        o.delete(1.0, 2.0)
    if o.index('end-1c')!='1.0':
        log.insert('end', '\n')
    o.insert('end', msg)
    o['state'] = 'disabled'


root = Tk()
root.title("Signieren von Hashwertbestaetigungen")

mainframe = ttk.Frame(root, padding="3 5 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)

Hashwert = StringVar();
Schluesseldatei = StringVar(); Schluesseldatei.set("None")
Datendatei = StringVar(); Datendatei.set("None")

ttk.Label(mainframe, text="Datei laden für Hashwertberechnung").grid(column=1, row=1, sticky=W)
ttk.Button(mainframe, text="Open", 
           command=HashwertAusDatei
          ).grid(column=2, row=1, sticky=W)
ttk.Label(mainframe, textvariable=Datendatei).grid(column=3, row=1, sticky=W)

ttk.Label(mainframe, text="Hashwert").grid(column=1, row=2, sticky=W)
Hashwert_entry = ttk.Entry(mainframe, width=64, textvariable=Hashwert)
Hashwert_entry.grid(column=2, row=2, sticky=(W, E))
ttk.Label(mainframe, text="(sha256)").grid(column=3, row=2, sticky=W)

ttk.Label(mainframe, text="privaten Schlüssel laden").grid(column=1, row=3, sticky=W)
ttk.Button(mainframe, text="Open", 
           command=LoadPrivKey
          ).grid(column=2, row=3, sticky=W)
ttk.Label(mainframe, textvariable=Schluesseldatei).grid(column=3, row=3, sticky=W)

ttk.Button(mainframe, text="Signatur erzeugen", command=Signaturerzeugung).grid(
    column=1, row=4, sticky=W
)

ttk.Label(mainframe, text="Ergebnis").grid(column=1, row=5, sticky=(N, E))
Output=Text(mainframe,  state='normal', width=80, height=4, wrap='none')
#Output.insert('1.0', 'Test Test Test')
Output.grid( column=2, row=5, sticky=(W, E))

for child in mainframe.winfo_children(): child.grid_configure(padx=5, pady=5)

Hashwert_entry.focus()
root.bind('<Return>', Signaturerzeugung)

root.mainloop()

