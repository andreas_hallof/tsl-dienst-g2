#! /usr/bin/env python3

import json, base64, argparse, os, sys, binascii
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization
from cryptography.exceptions import InvalidSignature
from cryptography import x509
from cryptography.x509.oid import NameOID


parser = argparse.ArgumentParser(description='Signaturbestaetigung erstellen')

# https://docs.python.org/3/library/argparse.html#required
parser.add_argument('-k', action="store", required=True, dest="schluesseldatei", help="pem-Datei mit dem privaten Schluessel")
parser.add_argument('-H', action="store", required=True, dest="hashwerttext", help="Hashwerttext à la e34323e...")

arg_results=parser.parse_args(); h=arg_results.hashwerttext; h=h.lower()

if len(h)!=64:
    N="Bestätigter Hashwert ({}) hat die falsche Länge (soll sha256-Hashwert sein).".format(h)
    print(N); sys.exit(1)
s=h
if len(s.strip("0123456789abcdef"))!=0:
    N="Bestätigter Hashwert ({}) ist nicht in die falsche Länge (soll sha256-Hashwert sein).".format(h)
    print(N); sys.exit(1)

Signaturbestaetigung= { 'Hashwerttext' : h, 'Signatur'     : '1' }

if not os.path.exists(arg_results.schluesseldatei):
     print("Kann Schluessldatei {} nicht finden.".format(arg_results.schluesseldatei))
     sys.exit(1)

with open(arg_results.schluesseldatei, "rb") as f:
    serialized_private=f.read()
    #print(serialized_private)

private_key = serialization.load_pem_private_key(
    serialized_private,
    password=None,
    backend=default_backend()
)

signature = private_key.sign( Signaturbestaetigung['Hashwerttext'].encode(), ec.ECDSA(hashes.SHA256()) )
Signaturbestaetigung['Signatur']=binascii.hexlify(signature)

#print(type(Signaturbestaetigung['Signatur']))
#print(Signaturbestaetigung['Signatur'])

signature=binascii.unhexlify(Signaturbestaetigung['Signatur'])
#print(type(signature))

public_key = private_key.public_key()
pn=public_key.public_numbers()
#print(pn.x, pn.y, pn.curve.name)
try:
    public_key.verify( signature, Signaturbestaetigung['Hashwerttext'].encode(), ec.ECDSA(hashes.SHA256()) )
except InvalidSignature:
    print("FAIL: das sollte nie passieren.")
    sys.exit(1)


zertname=arg_results.schluesseldatei
zertname=zertname.replace("key", "cert")
#print(zertname)
if os.path.exists(zertname):
    with open(zertname, "rb") as f:
        cert_data=f.read()

    #print(cert_data)
    cert = x509.load_pem_x509_certificate(cert_data, default_backend())
    public_key=cert.public_key()
    pn=public_key.public_numbers()
    #print(pn.x, pn.y, pn.curve.name)
    cn   = cert.subject.get_attributes_for_oid(NameOID.COMMON_NAME)[0].value

    assert isinstance(public_key, ec.EllipticCurvePublicKey)

    try:
        public_key.verify( signature, Signaturbestaetigung['Hashwerttext'].encode(), ec.ECDSA(hashes.SHA256()) )
    except InvalidSignature:
        print("FAIL: Falsche Zertifikat? (cn='{}')".format(cn))
        sys.exit(1)
else:
    print("INFO: {} nicht gefunden. Ich führe keine zusätzlichen Sanity-Check durch.".format(zertname))

Signaturbestaetigung['Signatur']=Signaturbestaetigung['Signatur'].decode()
print(json.dumps(Signaturbestaetigung, indent=4))


#with open("hh", "wb") as f:
#    f.write(signature)

