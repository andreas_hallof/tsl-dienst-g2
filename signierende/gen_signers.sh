#! /usr/bin/env bash

for i in $(seq 1 10); do

    echo $i
    #openssl ecparam -name brainpoolP256r1 -genkey -out key_$i.pem
    openssl ecparam -name prime256v1 -genkey -out key_$i.pem
    #strace openssl req -x509 -key key_$i.pem -out cert_$i.pem -days 365 -subj "/C=DE/ST=Berlin/L=Berlin/O=gemaik/OU=PoC/CN=TSL-Signaturbestaetiger $i"
    openssl req -x509 -key key_$i.pem -out cert_$i.pem -days 365 -subj "/C=DE/ST=Berlin/L=Berlin/O=gemaik/OU=PoC/CN=TSL-Signaturbestaetiger $i"

done

