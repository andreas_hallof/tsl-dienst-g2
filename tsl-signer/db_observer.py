#! /usr/bin/env python3

import os, sys, re, json, hashlib, binascii;
sys.path.append(os.getcwd())
sys.path.append(
    os.path.dirname(os.getcwd())+"/webinterface"
)

from ts_db_init import *
from web_db_init import *

import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.asymmetric import utils
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.exceptions import InvalidSignature

QUORUM=3

# https://stackoverflow.com/questions/18599339/python-watchdog-monitoring-file-for-changes
# http://brunorocha.org/python/watching-a-directory-for-file-changes-with-python.html

def MyLog(N):
    if TS_db.is_closed() or web_db.is_closed():
        print("Datenbank ist nicht connected.")
        print(N)
    else:
        q=TSProtokoll.insert(Nachricht=N); q.execute()
        q=Protokoll.insert(Nachricht="TS-Backend: "+N); q.execute()
        print(N)


def AuftraegeDurchsuchen():
    Verbindung_TS_DB(); Verbindung_Web_DB()

    Hash2Update=[]

    for i in Auftrag.select():
        print(i.id, i.Auftragsdaten[0:100])
        m=re.findall(b'^\s*\{', i.Auftragsdaten, flags=re.M+re.I)
        if len(m)==1:
            # json-data -> Hashwertbestätigung
            Signaturbestaetigung=json.loads(i.Auftragsdaten.decode())
            h=Signaturbestaetigung['Hashwerttext']
            h=h.lower()
            if len(h)!=64:
                MyLog("Bestätigter Hashwert ({}) hat die falsche Länge, ich ignoriere und verwerfe den Request".format(h))
                continue
            s=h
            if len(s.strip("0123456789abcdef"))!=0:
                MyLog("Bestätigter Hashwert ({}) ungültig, ich ignoriere und verwerfe den Request".format(h))
                continue

            signature=binascii.unhexlify(Signaturbestaetigung['Signatur'])

            for a in Zertifikat.select():
                cert = x509.load_pem_x509_certificate(a.cert, default_backend())
                cn   = cert.subject.get_attributes_for_oid(NameOID.COMMON_NAME)[0].value
                public_key=cert.public_key()
                pn=public_key.public_numbers()
                #print("Schlüssel", a.id, "mit cn=", cn, "und", pn.x, pn.y, pn.curve.name)

                assert isinstance(public_key, ec.EllipticCurvePublicKey)
                # todo cert.not_valid_before cert.not_valid_after
                try:
                    public_key.verify(signature, Signaturbestaetigung['Hashwerttext'].encode(), ec.ECDSA(hashes.SHA256()) )
                    MyLog("Erfolg: Schlüssel {} hat die Signatur ausgestellt".format(a.id));
                    Anzahl=HashwertOK.select().where(HashwertOK.Hashwert==h and HashwertOK.Schluesselid==a.id).count()
                    print("HH", Anzahl, h, a.id)
                    if Anzahl==0:
                        q=HashwertOK.insert(Hashwert=Signaturbestaetigung['Hashwerttext'],
                                            Auftragsdaten=i.Auftragsdaten,
                                            Signatur=Signaturbestaetigung['Signatur'],
                                            Schluesselid="{}".format(a.id),
                                            PubKey="{} {}".format(pn.x, pn.y)
                                           );
                        q.execute()
                        Hash2Update.append(Signaturbestaetigung['Hashwerttext'])
                    else:
                        MyLog("valid signierte Hashwertbestätigung ist schon einmal hochgeladen. -> ignoriere.")
                        for i in HashwertOK.select().where(HashwertOK.Hashwert==h and HashwertOK.Schluesselid==a.id):
                            print(i.date, i.Hashwert, i.Signatur)

                    break
                except InvalidSignature:
                    print("Schlüssel {} mit CN='{}' hat nicht die Signatur ausgestellt".format(a.id, cn))
        else:
            # Annahme Kandidat für TSL-Datei für das Hochladen zum Downloadpunkt
            h=hashlib.sha256(i.Auftragsdaten).hexdigest()
            Anzahl=HashwertOK.select().where(HashwertOK.Hashwert==h).count()
            if Anzahl>=QUORUM:
                MyLog("OK neue TSL ({}) wird hochgeladen.".format(h))
                with open("../webinterface/static/TSL.xml", "wb") as f:
                    f.write(i.Auftragsdaten)
                with open("../webinterface/static/TSL.xml.sha256", "wt") as f:
                    f.write(h + "\n")
                a=HashwertOK.select(HashwertOK.Signatur).where(HashwertOK.Hashwert==h and HashwertOK.Signatur!='');
                if len(a)==0:
                    print("UIII")
                else:
                    with open("../webinterface/static/TSL.xml.sig", "wt") as f:
                        f.write(a[0].Signatur + "\n")

            else:
                MyLog("Nicht genügend Hashwertebestätigungen für Hashwert {}, ignoriere und verwerfe den Request".format(h))

        MyLog('Lösche Auftrag mit Auftragsdaten "{}".'.format(i.Auftragsdaten)); 
        res=i.delete_instance()
        assert res==1


    for h in Hash2Update:
        Anzahl=HashwertOK.select().where(HashwertOK.Hashwert==h).count()
        if Anzahl==QUORUM:
            MyLog("Ich erstelle eine Signatur für den Hashwert {}.".format(h))

            with open("key_tsl_signer.pem", "rb") as f:
                serialized_private=f.read()
            private_key = serialization.load_pem_private_key( serialized_private, password=None, backend=default_backend())

            sig = private_key.sign(
                binascii.unhexlify(h),
                ec.ECDSA(utils.Prehashed(hashes.SHA256()))
            )

            hex_sig=binascii.hexlify(sig)

            q=HashwertOK.update(Signatur=hex_sig).where(HashwertOK.Hashwert==h and HashwertOK.Signatur!='')
            q.execute()

            MyLog("Signatur für {} ist {}.".format(h, hex_sig))

        elif Anzahl>QUORUM:
            a=HashwertOK.select().where(HashwertOK.Hashwert==h and HashwertOK.Signatur!='')
            if len(a)>0:
                x=a[0].Signatur
                q=HashwertOK.update(Signatur=x).where(HashwertOK.Hashwert==h and HashwertOK.Signatur!='')
                q.execute()
            else:
                print("uhhii sollte nicht passieren.")


    print("Hashwertbestätigungen {}, Zertifikate {}".format(
           HashwertOK.select().count(),
           Zertifikat.select().count()
    ))


    Schliesse_Web_DB(); Schliesse_TS_DB();

class MyHandler(FileSystemEventHandler):
    def on_modified(self, event):
        print("Got it!", event.src_path, event.event_type)
        if event.src_path == WEB_DB_FILE:
            print("OK jetzt geht die Party los.\nSleeping ...")
            time.sleep(1)

            AuftraegeDurchsuchen()

            print("Ready ... Returning")
        else:
            print("nothing to do")


if __name__ == "__main__":

    if not os.path.exists(WEB_DB_FILE):
        print("{} existiert nicht (db_init.py aufrufen).".format(WEB_DB_FILE))
        sys.exit(1)

    if not os.path.exists(TS_DB_FILE):
        print("{} existiert nicht (ts_db_init.py aufrufen).".format(TS_DB_FILE))
        sys.exit(1)


    AuftraegeDurchsuchen()

    #sys.exit(0)

    event_handler = MyHandler()
    observer = Observer()
    observer.schedule(event_handler, path='../webinterface', recursive=False)
    observer.start()

    try:
        print("Waiting for events")
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()

