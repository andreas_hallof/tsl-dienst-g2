#! /usr/bin/env bash


#openssl ecparam -name brainpoolP256r1 -genkey -out key_$i.pem
openssl ecparam -name prime256v1 -genkey -out key_tsl_signer.pem
openssl req -x509 -key key_tsl_signer.pem -out cert_tsl_signer.pem -days 365 -subj "/C=DE/ST=Berlin/L=Berlin/O=gemaik/OU=PoC/CN=TSL-Signer"

