#! /usr/bin/env python3

import os, sys
from peewee import *
import datetime


TS_DB_FILE="TSL_Signer.db"
TS_db = SqliteDatabase(TS_DB_FILE)

class BaseModel(Model):
    class Meta:
        database = TS_db

class HashwertOK(BaseModel):
    id            = PrimaryKeyField()
    date          = DateTimeField(default = datetime.datetime.now)
    Hashwert      = CharField()
    Auftragsdaten = CharField()
    Signatur      = CharField()
    Schluesselid  = CharField()
    PubKey        = CharField()

class TSProtokoll(BaseModel):
    id          = PrimaryKeyField()
    date        = DateTimeField(default = datetime.datetime.now)
    Nachricht   = CharField()

# autorisierte "signierende" Bestätigungszertifikate
class Zertifikat(BaseModel):
    id          = PrimaryKeyField()
    date        = DateTimeField(default = datetime.datetime.now)
    cert        = BlobField()

def Verbindung_TS_DB():
    TS_db.connect()

def Schliesse_TS_DB():
    TS_db.close()

if __name__ == '__main__':

    Verbindung_TS_DB()

    HashwertOK.drop_table()  if HashwertOK.table_exists() else ""
    TSProtokoll.drop_table() if TSProtokoll.table_exists() else ""
    Zertifikat.drop_table()  if Zertifikat.table_exists() else ""

    TS_db.create_tables([HashwertOK, TSProtokoll, Zertifikat])

    q=TSProtokoll.insert(Nachricht="Neuinitialiserung der TS-Datenbank")
    q.execute()

    for i in os.listdir("../signierende"):
        a="../signierende/"+i
        if all([ os.path.isfile(a), i.startswith("cert"), i.endswith(".pem") ]):
            with open(a,"rt") as f:
                c=f.read()
                q=Zertifikat.insert(cert=c)
                q.execute()

    print([ x.id for x in Zertifikat.select() ])


