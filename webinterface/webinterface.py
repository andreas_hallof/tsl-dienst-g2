#! /usr/bin/env python3

import os, sys; sys.path.append(os.getcwd())
from flask import Flask, render_template, request, send_from_directory, redirect, Response
from web_db_init import *

app = Flask(__name__)

@app.before_request
def before_request():
    Verbindung_Web_DB()

@app.teardown_request
def teardown_request(exception):
    Schliesse_Web_DB()

@app.route('/')
@app.route('/tsl', methods=['GET', 'POST'])
def entry_point_tsl():

    MyDebug="hi"

    if request.method == 'POST' and 'Datei' in request.files:
        MyDebug="post"
        f=request.files['Datei']
        if f.filename:
            MyDebug=f.filename
            # hier muss noch etwas rein, damit nicht jemand zig GB hochlädt.
            # da ich hier hinter einem haproxy mit basicauth laufe ist das problem
            # nicht so groß, aber ist trotzdem ein Thema.
            # Auch ein Thema ... 10**6 request mit einem Upload (auch kleiner als eine
            # maximale Größe) ... haproxy
            #
            # Da ich tsl-upload-request sofort lösche falls es nicht genügend Hashwert-
            # Bestätigungen in der DB gibt, ist das Problem nochmal geringer (und immernoch
            # > 0).
            q=Auftrag.insert(Auftragstyp='', Auftragsdaten=f.read())
            q.execute()
        if request.form.get('HashwertBest'):
            q=Auftrag.insert(Auftragstyp='', Auftragsdaten=request.form.get('HashwertBest'))
            q.execute()

    return render_template("tsl.html",
                           Auftrag=Auftrag.select(),
                           Protokoll=Protokoll.select().order_by(-Protokoll.date),
                           MyDebug=MyDebug
                          )

@app.route('/log.txt')
def serve_logfile():
    LogData=""
    for i in Protokoll.select().order_by(-Protokoll.date):
        LogData+="{} | {} | {}\n".format(i.id, i.date, i.Nachricht)

    return Response(LogData, mimetype='text/plain')

# https://stackoverflow.com/questions/5870188/does-flask-support-regular-expressions-in-its-url-routing

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/TSL.xml')
def tslxml():
    return send_from_directory(os.path.join(app.root_path, 'static'),'TSL.xml', mimetype='application/xml')

@app.route('/TSL.xml.sig')
def tslxmlsig():
    return send_from_directory(os.path.join(app.root_path, 'static'),'TSL.xml.sig', mimetype='application/octet-stream')

@app.route('/TSL.xml.sha256')
def tslxmlsha256():
    return send_from_directory(os.path.join(app.root_path, 'static'),'TSL.xml.sha256', mimetype='text/plain')

@app.route('/tksign.zip')
def signaturtoolzip():
    return send_from_directory(os.path.join(app.root_path, 'static'),'tksign.zip', mimetype='application/zip')

@app.route('/cert_tsl_signer.pem')
def cert_tsl_signer():
    return send_from_directory(os.path.join(app.root_path, 'static'),'cert_tsl_signer.pem', mimetype='text/plain')


if __name__ == '__main__':

    MY_PORT=9011
    if os.getenv('HOSTNAME')=='b':
        MY_DEBUG=False
    else:
        MY_DEBUG=True

    # merker 
    #import socket
    #print(socket.gethostname())

    if not os.path.exists(WEB_DB_FILE):
        print("{} existiert nicht (db_init.py aufrufen).".format(WEB_DB_FILE))
        sys.exit(1)
    
    # xxx im Produktivbetrieb natuerlich mit debug=False
    app.run( port=MY_PORT, debug=MY_DEBUG )

