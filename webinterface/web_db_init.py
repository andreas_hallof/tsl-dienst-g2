#! /usr/bin/env python3

import datetime, os, sys
from peewee import *


WEB_DB_FILE="WebInt.db"
if not os.path.exists(WEB_DB_FILE):
    WEB_DB_FILE="../webinterface/"+WEB_DB_FILE

if not os.path.exists(WEB_DB_FILE):
    print("WEB DB file not found")
    sys.exit(1)

web_db = SqliteDatabase(WEB_DB_FILE)

class BaseModel(Model):
    class Meta:
        database = web_db

class Auftrag(BaseModel):
    id = PrimaryKeyField()
    date = DateTimeField(default = datetime.datetime.now)
    Auftragstyp=CharField()
    Auftragsdaten=BlobField()

class Protokoll(BaseModel):
    id = PrimaryKeyField()
    date = DateTimeField(default = datetime.datetime.now)
    Nachricht=CharField()

def Verbindung_Web_DB():
    web_db.connect()

def Schliesse_Web_DB():
    web_db.close()

if __name__ == '__main__':

    Verbindung_Web_DB()

    if Auftrag.table_exists():
        Auftrag.drop_table()
    Auftrag.create_table() # merker alternativ: avs_db.create_tables([Auftrag])

    if Protokoll.table_exists():
        Protokoll.drop_table()
    Protokoll.create_table()

    #for x in Karte.select():
    #    print(x.cardhandle, x.karteninhaber)

